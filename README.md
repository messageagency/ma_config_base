# README #

This modules installs new and updates existing configuration, providing a base for the ma_sprout profile.

It should be enabled automatically when installing a site using the ma_sprout profile. When it is enabled, it imports config and serves no purpose after that.